package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataController {

	@GetMapping("/calcular/{quantity}/{factor}")
	public DataBean calcular(@PathVariable Double quantity, @PathVariable Double factor) {
		if (quantity != null && factor != null) {
			return new DataBean(quantity, factor);
		}
		return new DataBean();
	}
}
